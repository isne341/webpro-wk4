function reValidate(el, re){
    return re.test(el)
}            
function validateName(){
    var name = document.forms["register"]["forename"].value
    var name_box = document.getElementById("forename")
    var re = /^[A-z]{3,}$/;
    //check name's condition to alter name_box's border color
    //same as multiple ternary statements below
    reValidate(name, re)?(name_box.style.borderColor = "green"):(name_box.style.borderColor = "red", document.getElementById("error-text").innerHTML = "Name must contain atleast 3 alphabets, no spaces.")
}
            
function validateSname(){
    var s_name = document.forms["register"]["surname"].value
    var s_name_box = document.getElementById("surname")
    var re = /^[A-z]{3,}$/;
    
    reValidate(s_name, re)?(s_name_box.style.borderColor = "green"):(s_name_box.style.borderColor = "red",document.getElementById("error-text").innerHTML = "Name must contain atleast 3 alphabets, no spaces.")
}
            
function validateUsers(){
    var user = document.forms["register"]["username"].value
    var user_box = document.getElementById("username")
    var re = /^[A-z0-9_-]{5,}$/;
    
    reValidate(user, re)?(user_box.style.borderColor = "green"):(user_box.style.borderColor = "red", document.getElementById("error-text").innerHTML = "Username must have at least 5 characters, can include numbers, _ and -.")
}
            
function validatePass(){
    var pass = document.forms["register"]["password"].value
    var pass_box = document.getElementById("password")
    var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8/
         
    reValidate(pass, re)?pass_box.style.borderColor = "green":(pass_box.style.borderColor = "red", document.getElementById("error-text").innerHTML = "Password must be at least 8 characters, containing uppercase, lowercase, number and symbol.")

}
function validateAge(){
    var age = document.forms["register"]["age"].value
    var age_box = document.getElementById("age")
    //check if age is less than 110 first, then check if age is more than 18
    age <= 110?(age >= 18?age_box.style.borderColor = "green":(document.getElementById("error-text").innerHTML = "You must be at least 18 to register", age_box.style.borderColor = "red")):(document.getElementById("error-text").innerHTML = "You're too old for this", age_box.style.borderColor = "red")
}
